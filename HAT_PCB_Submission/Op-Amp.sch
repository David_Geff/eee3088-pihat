EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date "2021-06-02"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:VSS #PWR0106
U 1 1 60B6F822
P 2250 3500
F 0 "#PWR0106" H 2250 3350 50  0001 C CNN
F 1 "VSS" H 2268 3673 50  0000 C CNN
F 2 "" H 2250 3500 50  0001 C CNN
F 3 "" H 2250 3500 50  0001 C CNN
	1    2250 3500
	-1   0    0    1   
$EndComp
Text Label 1650 3100 0    50   ~ 0
Vin
Wire Wire Line
	1650 3100 2050 3100
$Comp
L Device:R_Small_US R18
U 1 1 60B716BC
P 3250 3650
F 0 "R18" H 3318 3696 50  0000 L CNN
F 1 "12k" H 3318 3605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3250 3650 50  0001 C CNN
F 3 "~" H 3250 3650 50  0001 C CNN
	1    3250 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R19
U 1 1 60B73513
P 3250 4250
F 0 "R19" H 3318 4296 50  0000 L CNN
F 1 "10k" H 3318 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 3250 4250 50  0001 C CNN
F 3 "~" H 3250 4250 50  0001 C CNN
	1    3250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3950 1850 3300
Wire Wire Line
	1850 3300 2050 3300
$Comp
L power:GND #PWR0107
U 1 1 60B74732
P 3250 4600
F 0 "#PWR0107" H 3250 4350 50  0001 C CNN
F 1 "GND" H 3255 4427 50  0000 C CNN
F 2 "" H 3250 4600 50  0001 C CNN
F 3 "" H 3250 4600 50  0001 C CNN
	1    3250 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3950 3250 3950
Wire Wire Line
	3250 3950 3250 3750
Wire Wire Line
	3250 4150 3250 3950
Connection ~ 3250 3950
Wire Wire Line
	3250 4600 3250 4350
Wire Wire Line
	3250 3550 3250 3200
Wire Wire Line
	3250 3200 2650 3200
Wire Wire Line
	3250 3200 3500 3200
Connection ~ 3250 3200
$Comp
L Amplifier_Operational:OP179GRT U3
U 1 1 60B91C25
P 5450 3300
F 0 "U3" H 5794 3346 50  0000 L CNN
F 1 "OP179GRT" H 5794 3255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5450 3300 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 5450 3500 50  0001 C CNN
	1    5450 3300
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR0108
U 1 1 60B91C31
P 5350 3600
F 0 "#PWR0108" H 5350 3450 50  0001 C CNN
F 1 "VSS" H 5368 3773 50  0000 C CNN
F 2 "" H 5350 3600 50  0001 C CNN
F 3 "" H 5350 3600 50  0001 C CNN
	1    5350 3600
	-1   0    0    1   
$EndComp
Text Label 4750 3200 0    50   ~ 0
Vin
Wire Wire Line
	4750 3200 5150 3200
$Comp
L Device:R_Small_US R20
U 1 1 60B91C39
P 6350 3750
F 0 "R20" H 6418 3796 50  0000 L CNN
F 1 "12k" H 6418 3705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6350 3750 50  0001 C CNN
F 3 "~" H 6350 3750 50  0001 C CNN
	1    6350 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R21
U 1 1 60B91C3F
P 6350 4350
F 0 "R21" H 6418 4396 50  0000 L CNN
F 1 "10k" H 6418 4305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 6350 4350 50  0001 C CNN
F 3 "~" H 6350 4350 50  0001 C CNN
	1    6350 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4050 4950 3400
Wire Wire Line
	4950 3400 5150 3400
$Comp
L power:GND #PWR0109
U 1 1 60B91C47
P 6350 4700
F 0 "#PWR0109" H 6350 4450 50  0001 C CNN
F 1 "GND" H 6355 4527 50  0000 C CNN
F 2 "" H 6350 4700 50  0001 C CNN
F 3 "" H 6350 4700 50  0001 C CNN
	1    6350 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4050 6350 4050
Wire Wire Line
	6350 4050 6350 3850
Wire Wire Line
	6350 4250 6350 4050
Connection ~ 6350 4050
Wire Wire Line
	6350 4700 6350 4450
Wire Wire Line
	6350 3650 6350 3300
Wire Wire Line
	6350 3300 5750 3300
Wire Wire Line
	6350 3300 6600 3300
Connection ~ 6350 3300
$Comp
L Amplifier_Operational:OP179GRT U4
U 1 1 60B93249
P 8500 3400
F 0 "U4" H 8844 3446 50  0000 L CNN
F 1 "OP179GRT" H 8844 3355 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 8500 3400 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 8500 3600 50  0001 C CNN
	1    8500 3400
	1    0    0    -1  
$EndComp
$Comp
L power:VSS #PWR0110
U 1 1 60B93255
P 8400 3700
F 0 "#PWR0110" H 8400 3550 50  0001 C CNN
F 1 "VSS" H 8418 3873 50  0000 C CNN
F 2 "" H 8400 3700 50  0001 C CNN
F 3 "" H 8400 3700 50  0001 C CNN
	1    8400 3700
	-1   0    0    1   
$EndComp
Text Label 7800 3300 0    50   ~ 0
Vin
Wire Wire Line
	7800 3300 8200 3300
$Comp
L Device:R_Small_US R22
U 1 1 60B9325D
P 9400 3850
F 0 "R22" H 9468 3896 50  0000 L CNN
F 1 "12k" H 9468 3805 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 9400 3850 50  0001 C CNN
F 3 "~" H 9400 3850 50  0001 C CNN
	1    9400 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R23
U 1 1 60B93263
P 9400 4450
F 0 "R23" H 9468 4496 50  0000 L CNN
F 1 "10k" H 9468 4405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" H 9400 4450 50  0001 C CNN
F 3 "~" H 9400 4450 50  0001 C CNN
	1    9400 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4150 8000 3500
Wire Wire Line
	8000 3500 8200 3500
$Comp
L power:GND #PWR0111
U 1 1 60B9326B
P 9400 4800
F 0 "#PWR0111" H 9400 4550 50  0001 C CNN
F 1 "GND" H 9405 4627 50  0000 C CNN
F 2 "" H 9400 4800 50  0001 C CNN
F 3 "" H 9400 4800 50  0001 C CNN
	1    9400 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4150 9400 4150
Wire Wire Line
	9400 4150 9400 3950
Wire Wire Line
	9400 4350 9400 4150
Connection ~ 9400 4150
Wire Wire Line
	9400 4800 9400 4550
Wire Wire Line
	9400 3750 9400 3400
Wire Wire Line
	9400 3400 8800 3400
Wire Wire Line
	9400 3400 9650 3400
Connection ~ 9400 3400
Text Notes 2100 2600 0    50   ~ 0
Motion Sensor Input
Text Notes 5150 2700 0    50   ~ 0
Stability Sensor Input
Text Notes 8150 2750 0    50   ~ 0
Magnetometer Input
Text Notes 7500 7500 0    50   ~ 0
Operational Amplifier
Text Notes 8200 7650 0    50   ~ 0
01/06/2021
Text Notes 10600 7650 0    50   ~ 0
0\n
Text GLabel 2250 2900 0    50   Input ~ 0
5V_USB
$Comp
L Amplifier_Operational:OP179GRT U2
U 1 1 60B6D7C5
P 2350 3200
F 0 "U2" H 2694 3246 50  0000 L CNN
F 1 "OP179GRT" H 2694 3155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 2350 3200 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/OP179_279.pdf" H 2350 3400 50  0001 C CNN
	1    2350 3200
	1    0    0    -1  
$EndComp
Text GLabel 5350 3000 0    50   Input ~ 0
5V_USB
Text GLabel 8400 3100 0    50   Input ~ 0
5V_USB
Text HLabel 3500 3200 2    50   Input ~ 0
Motion_out
Text HLabel 6600 3300 2    50   Input ~ 0
Stability_out
Text HLabel 9650 3400 2    50   Input ~ 0
Magneto_out
$EndSCHEMATC
