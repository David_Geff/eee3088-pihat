EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "LED Subsystem"
Date "2021-06-02"
Rev "1"
Comp "Abu Bakr Salie"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D1
U 1 1 60C1F4E5
P 2150 4200
F 0 "D1" V 2189 4082 50  0000 R CNN
F 1 "LED" V 2098 4082 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 2150 4200 50  0001 C CNN
F 3 "~" H 2150 4200 50  0001 C CNN
	1    2150 4200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 60C1F4EB
P 2150 3800
F 0 "R1" H 2220 3846 50  0000 L CNN
F 1 "10" H 2220 3755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2080 3800 50  0001 C CNN
F 3 "~" H 2150 3800 50  0001 C CNN
	1    2150 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60C1F4F1
P 2700 4750
F 0 "R2" V 2493 4750 50  0000 C CNN
F 1 "1000" V 2584 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2630 4750 50  0001 C CNN
F 3 "~" H 2700 4750 50  0001 C CNN
	1    2700 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 4450 2150 4350
Wire Wire Line
	2150 4050 2150 3950
$Comp
L Device:LED D2
U 1 1 60B85FB1
P 3450 4350
F 0 "D2" V 3489 4232 50  0000 R CNN
F 1 "LED" V 3398 4232 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 3450 4350 50  0001 C CNN
F 3 "~" H 3450 4350 50  0001 C CNN
	1    3450 4350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 60B85FB7
P 3450 3950
F 0 "R3" H 3520 3996 50  0000 L CNN
F 1 "10" H 3520 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3380 3950 50  0001 C CNN
F 3 "~" H 3450 3950 50  0001 C CNN
	1    3450 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60B85FBD
P 4000 4900
F 0 "R4" V 3793 4900 50  0000 C CNN
F 1 "1000" V 3884 4900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3930 4900 50  0001 C CNN
F 3 "~" H 4000 4900 50  0001 C CNN
	1    4000 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 4600 3450 4500
Wire Wire Line
	3450 4200 3450 4100
$Comp
L Device:LED D3
U 1 1 60B87C8B
P 4750 4150
F 0 "D3" V 4789 4032 50  0000 R CNN
F 1 "LED" V 4698 4032 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 4750 4150 50  0001 C CNN
F 3 "~" H 4750 4150 50  0001 C CNN
	1    4750 4150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 60B87C91
P 4750 3750
F 0 "R5" H 4820 3796 50  0000 L CNN
F 1 "10" H 4820 3705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4680 3750 50  0001 C CNN
F 3 "~" H 4750 3750 50  0001 C CNN
	1    4750 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 60B87C97
P 5300 4700
F 0 "R6" V 5093 4700 50  0000 C CNN
F 1 "R1000" V 5184 4700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5230 4700 50  0001 C CNN
F 3 "~" H 5300 4700 50  0001 C CNN
	1    5300 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 4400 4750 4300
Wire Wire Line
	4750 4000 4750 3900
$Comp
L Device:LED D4
U 1 1 60B89583
P 5900 4350
F 0 "D4" V 5939 4232 50  0000 R CNN
F 1 "LED" V 5848 4232 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 5900 4350 50  0001 C CNN
F 3 "~" H 5900 4350 50  0001 C CNN
	1    5900 4350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 60B89589
P 5900 3950
F 0 "R7" H 5970 3996 50  0000 L CNN
F 1 "10" H 5970 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5830 3950 50  0001 C CNN
F 3 "~" H 5900 3950 50  0001 C CNN
	1    5900 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 60B8958F
P 6450 4900
F 0 "R8" V 6243 4900 50  0000 C CNN
F 1 "R1000" V 6334 4900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6380 4900 50  0001 C CNN
F 3 "~" H 6450 4900 50  0001 C CNN
	1    6450 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	5900 4600 5900 4500
Wire Wire Line
	5900 4200 5900 4100
$Comp
L Device:LED D5
U 1 1 60B8B769
P 6900 4000
F 0 "D5" V 6939 3882 50  0000 R CNN
F 1 "LED" V 6848 3882 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 6900 4000 50  0001 C CNN
F 3 "~" H 6900 4000 50  0001 C CNN
	1    6900 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R9
U 1 1 60B8B76F
P 6900 3600
F 0 "R9" H 6970 3646 50  0000 L CNN
F 1 "10" H 6970 3555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6830 3600 50  0001 C CNN
F 3 "~" H 6900 3600 50  0001 C CNN
	1    6900 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 60B8B775
P 7450 4550
F 0 "R10" V 7243 4550 50  0000 C CNN
F 1 "1000" V 7334 4550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7380 4550 50  0001 C CNN
F 3 "~" H 7450 4550 50  0001 C CNN
	1    7450 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 4250 6900 4150
Wire Wire Line
	6900 3850 6900 3750
$Comp
L Device:LED D6
U 1 1 60B8D399
P 8000 4450
F 0 "D6" V 8039 4332 50  0000 R CNN
F 1 "LED" V 7948 4332 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8000 4450 50  0001 C CNN
F 3 "~" H 8000 4450 50  0001 C CNN
	1    8000 4450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R11
U 1 1 60B8D39F
P 8000 4050
F 0 "R11" H 8070 4096 50  0000 L CNN
F 1 "10" H 8070 4005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7930 4050 50  0001 C CNN
F 3 "~" H 8000 4050 50  0001 C CNN
	1    8000 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 4700 8000 4600
Wire Wire Line
	8000 4300 8000 4200
$Comp
L Device:LED D7
U 1 1 60B8F715
P 8900 4000
F 0 "D7" V 8939 3882 50  0000 R CNN
F 1 "LED" V 8848 3882 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8900 4000 50  0001 C CNN
F 3 "~" H 8900 4000 50  0001 C CNN
	1    8900 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R13
U 1 1 60B8F71B
P 8900 3600
F 0 "R13" H 8970 3646 50  0000 L CNN
F 1 "R10" H 8970 3555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 8830 3600 50  0001 C CNN
F 3 "~" H 8900 3600 50  0001 C CNN
	1    8900 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 60B8F721
P 9450 4550
F 0 "R14" V 9243 4550 50  0000 C CNN
F 1 "1000" V 9334 4550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 9380 4550 50  0001 C CNN
F 3 "~" H 9450 4550 50  0001 C CNN
	1    9450 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	8900 4250 8900 4150
Wire Wire Line
	8900 3850 8900 3750
Text GLabel 2100 3400 0    50   Input ~ 0
5V_USB
Wire Wire Line
	1900 5550 2150 5550
Wire Wire Line
	2150 5550 2150 5050
Wire Wire Line
	8900 4850 8900 5550
Wire Wire Line
	8000 5300 8000 5550
Wire Wire Line
	6900 4850 6900 5550
Connection ~ 6900 5550
Wire Wire Line
	5900 5200 5900 5550
Connection ~ 5900 5550
Wire Wire Line
	5900 5550 6900 5550
Wire Wire Line
	4750 5000 4750 5550
Connection ~ 4750 5550
Wire Wire Line
	4750 5550 5900 5550
Wire Wire Line
	3450 5200 3450 5550
Connection ~ 3450 5550
Wire Wire Line
	3450 5550 4750 5550
Text HLabel 2850 4750 2    50   Input ~ 0
LED1(GPIO5)
Text HLabel 4150 4900 2    50   Input ~ 0
LED2(GPIO6)
Text HLabel 5450 4700 2    50   Input ~ 0
LED3(GPIO13)
Text HLabel 6600 4900 2    50   Input ~ 0
LED4(GPIO19)
Text HLabel 7600 4550 2    50   Input ~ 0
LED5(GPIO26)
Text HLabel 9600 4550 2    50   Input ~ 0
LED7(GPIO20)
Wire Wire Line
	8900 3400 8900 3450
Wire Wire Line
	8000 3900 8000 3400
Wire Wire Line
	6900 3450 6900 3400
Connection ~ 6900 3400
Wire Wire Line
	5900 3400 5900 3800
Connection ~ 5900 3400
Wire Wire Line
	5900 3400 6900 3400
Wire Wire Line
	4750 3600 4750 3400
Wire Wire Line
	2100 3400 2150 3400
Connection ~ 4750 3400
Wire Wire Line
	4750 3400 5900 3400
Wire Wire Line
	3450 3400 3450 3800
Connection ~ 3450 3400
Wire Wire Line
	3450 3400 4750 3400
Wire Wire Line
	2150 3650 2150 3400
Connection ~ 8000 3400
Wire Wire Line
	8000 3400 8900 3400
Connection ~ 8000 5550
Wire Wire Line
	8000 5550 8900 5550
Wire Wire Line
	6900 5550 8000 5550
Wire Wire Line
	6900 3400 8000 3400
$Comp
L Device:R R12
U 1 1 60BCA897
P 8550 5000
F 0 "R12" V 8343 5000 50  0000 C CNN
F 1 "1000" V 8434 5000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 8480 5000 50  0001 C CNN
F 3 "~" H 8550 5000 50  0001 C CNN
	1    8550 5000
	0    1    1    0   
$EndComp
Text HLabel 8700 5000 2    50   Input ~ 0
LED6(GPIO21)
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q1
U 1 1 60B9204D
P 2550 4750
AR Path="/60B9204D" Ref="Q1"  Part="1" 
AR Path="/60B89E55/60B9204D" Ref="Q1"  Part="1" 
F 0 "Q1" H 3088 4796 50  0000 L CNN
F 1 "2N2222AUB" H 3088 4705 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 3100 4600 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 3100 4500 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 3100 4400 50  0001 L CNN "Description"
F 5 "2.16" H 3100 4300 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 3100 4200 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 3100 4100 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 3100 4000 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 3100 3900 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 3100 3800 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 3100 3700 50  0001 L CNN "Arrow Price/Stock"
	1    2550 4750
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q2
U 1 1 60B945CA
P 3850 4900
AR Path="/60B945CA" Ref="Q2"  Part="1" 
AR Path="/60B89E55/60B945CA" Ref="Q2"  Part="1" 
F 0 "Q2" H 4388 4946 50  0000 L CNN
F 1 "2N2222AUB" H 4388 4855 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 4400 4750 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 4400 4650 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 4400 4550 50  0001 L CNN "Description"
F 5 "2.16" H 4400 4450 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 4400 4350 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 4400 4250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 4400 4150 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 4400 4050 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 4400 3950 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 4400 3850 50  0001 L CNN "Arrow Price/Stock"
	1    3850 4900
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q3
U 1 1 60B95350
P 5150 4700
AR Path="/60B95350" Ref="Q3"  Part="1" 
AR Path="/60B89E55/60B95350" Ref="Q3"  Part="1" 
F 0 "Q3" H 5688 4746 50  0000 L CNN
F 1 "2N2222AUB" H 5688 4655 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 5700 4550 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 5700 4450 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 5700 4350 50  0001 L CNN "Description"
F 5 "2.16" H 5700 4250 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 5700 4150 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 5700 4050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 5700 3950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 5700 3850 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 5700 3750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 5700 3650 50  0001 L CNN "Arrow Price/Stock"
	1    5150 4700
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q4
U 1 1 60B964F8
P 6300 4900
AR Path="/60B964F8" Ref="Q4"  Part="1" 
AR Path="/60B89E55/60B964F8" Ref="Q4"  Part="1" 
F 0 "Q4" H 6838 4946 50  0000 L CNN
F 1 "2N2222AUB" H 6838 4855 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 6850 4750 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 6850 4650 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 6850 4550 50  0001 L CNN "Description"
F 5 "2.16" H 6850 4450 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 6850 4350 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 6850 4250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 6850 4150 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 6850 4050 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 6850 3950 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 6850 3850 50  0001 L CNN "Arrow Price/Stock"
F 12 "Q" H 6300 4900 50  0001 C CNN "Spice_Primitive"
F 13 "2N2222AUB" H 6300 4900 50  0001 C CNN "Spice_Model"
F 14 "Y" H 6300 4900 50  0001 C CNN "Spice_Netlist_Enabled"
	1    6300 4900
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q5
U 1 1 60B97407
P 7300 4550
AR Path="/60B97407" Ref="Q5"  Part="1" 
AR Path="/60B89E55/60B97407" Ref="Q5"  Part="1" 
F 0 "Q5" H 7838 4596 50  0000 L CNN
F 1 "2N2222AUB" H 7838 4505 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 7850 4400 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 7850 4300 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 7850 4200 50  0001 L CNN "Description"
F 5 "2.16" H 7850 4100 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 7850 4000 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 7850 3900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 7850 3800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 7850 3700 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 7850 3600 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 7850 3500 50  0001 L CNN "Arrow Price/Stock"
	1    7300 4550
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q6
U 1 1 60B97DCB
P 8400 5000
AR Path="/60B97DCB" Ref="Q6"  Part="1" 
AR Path="/60B89E55/60B97DCB" Ref="Q6"  Part="1" 
F 0 "Q6" H 8938 5046 50  0000 L CNN
F 1 "2N2222AUB" H 8938 4955 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 8950 4850 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 8950 4750 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 8950 4650 50  0001 L CNN "Description"
F 5 "2.16" H 8950 4550 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 8950 4450 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 8950 4350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 8950 4250 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 8950 4150 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 8950 4050 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 8950 3950 50  0001 L CNN "Arrow Price/Stock"
	1    8400 5000
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q7
U 1 1 60B98B50
P 9300 4550
AR Path="/60B98B50" Ref="Q7"  Part="1" 
AR Path="/60B89E55/60B98B50" Ref="Q7"  Part="1" 
F 0 "Q7" H 9838 4596 50  0000 L CNN
F 1 "2N2222AUB" H 9838 4505 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 9850 4400 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 9850 4300 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 9850 4200 50  0001 L CNN "Description"
F 5 "2.16" H 9850 4100 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 9850 4000 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 9850 3900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 9850 3800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 9850 3700 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 9850 3600 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 9850 3500 50  0001 L CNN "Arrow Price/Stock"
	1    9300 4550
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 60B9AD87
P 1900 5550
F 0 "#PWR0116" H 1900 5300 50  0001 C CNN
F 1 "GND" H 1905 5377 50  0000 C CNN
F 2 "" H 1900 5550 50  0001 C CNN
F 3 "" H 1900 5550 50  0001 C CNN
	1    1900 5550
	1    0    0    -1  
$EndComp
Connection ~ 2150 3400
Connection ~ 2150 5550
Wire Wire Line
	2150 5550 3450 5550
Wire Wire Line
	2150 3400 3450 3400
$EndSCHEMATC
