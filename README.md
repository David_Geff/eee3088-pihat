# EEE3088 PiHAT

We are designing a multi-sensor microPI HAT module (including a telemetry sensor made up of accelerometers, gyroscopes and magnetometers) that will be attached to a Raspberry Pi Zero and powered externally for a range of potential applications. These include: velocity and acceleration tracking for remote controlled ground vehicles (such as RC cars), gyroscopic stabilization for quadcopter drones during flight and remotely searching for faults in heavy current electrical systems in hard to reach places by sensing irregularities in the expected magnetic field in an area.

The HAT must meet the following requirements:

Standard requirements for microPI HATs:
microPI HATs must follow all of the standard electrical HAT rules as laid out for normal HATs, the only difference being that they have a smaller mechanical form factor as seen in the diagram below
Must conform to the basic add-on requirements of a HAT
Must have a full size 40W GPIO connector
Must follow the HAT mechanical specification as seen in the diagram above (https://github.com/raspberrypi/hats/blob/master/uhat-board-mechanical.pdf)
Must use spacers 8mm or larger
Must be able to supply 2A continuously to the PI
Must have a correctly implemented ZVD’ safety circuit

Motion Sensor (velocity and acceleration tracking) microPI HAT requirements:
R1.1: Must be able to log data to local storage
R1.2: Must be able to track high acceleration
R1.3: Must be able to track the position of the vehicle in 2 dimensions
R1.4: Must be small enough to be mounted onto the remotely controlled vehicle
R1.5: Must be lightweight enough to not impact the vehicles handling  capabilities 
R1.6: Must be able to withstand the force of impact experienced if the remotely controlled vehicle were to crash


Stability Sensor (gyroscopic stabilization) microPI HAT requirements:
R2.1:  Must be able to detect slight adjustments in the orientation of the drone
R2.2: Must be able to track the position of the drone in 3 dimensions
R2.3: Must be small enough to be mounted on the quadcopter drones (which themselves are quite small in size)
R2.4: Must be lightweight so as not to impact the drones’ flying capabilities 
R2.5: Must be hardy enough, as well as must be fastened securely, to be able to withstand the air resistance that will be experienced whilst the drone is being flown.
R2.6: Must be able to report disturbances in real time

Magnetometer (magnetic field irregularity sensing) Sensor microPI HAT requirements: 
R3.1: Must have variable sensitivity for different use cases
R3.2: Must be resistant to internal interference
R3.3: Must be small enough to be mounted on many different unmanned craft (drones etc.)
R3.4: Must be able to withstand the effects of being placed in a strong magnetic field
