EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "LED Subsystem"
Date "2021-06-02"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D1
U 1 1 60C1F4E5
P 1700 2850
F 0 "D1" V 1739 2732 50  0000 R CNN
F 1 "LED" V 1648 2732 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 1700 2850 50  0001 C CNN
F 3 "~" H 1700 2850 50  0001 C CNN
	1    1700 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 60C1F4EB
P 1700 2450
F 0 "R1" H 1770 2496 50  0000 L CNN
F 1 "10" H 1770 2405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 1630 2450 50  0001 C CNN
F 3 "~" H 1700 2450 50  0001 C CNN
	1    1700 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60C1F4F1
P 2250 3400
F 0 "R2" V 2043 3400 50  0000 C CNN
F 1 "1000" V 2134 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2180 3400 50  0001 C CNN
F 3 "~" H 2250 3400 50  0001 C CNN
	1    2250 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 3100 1700 3000
Wire Wire Line
	1700 2700 1700 2600
$Comp
L Device:LED D2
U 1 1 60B85FB1
P 2700 3000
F 0 "D2" V 2739 2882 50  0000 R CNN
F 1 "LED" V 2648 2882 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 2700 3000 50  0001 C CNN
F 3 "~" H 2700 3000 50  0001 C CNN
	1    2700 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 60B85FB7
P 2700 2600
F 0 "R3" H 2770 2646 50  0000 L CNN
F 1 "10" H 2770 2555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2630 2600 50  0001 C CNN
F 3 "~" H 2700 2600 50  0001 C CNN
	1    2700 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 60B85FBD
P 3250 3550
F 0 "R4" V 3043 3550 50  0000 C CNN
F 1 "1000" V 3134 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3180 3550 50  0001 C CNN
F 3 "~" H 3250 3550 50  0001 C CNN
	1    3250 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	2700 3250 2700 3150
Wire Wire Line
	2700 2850 2700 2750
$Comp
L Device:LED D3
U 1 1 60B87C8B
P 4000 2800
F 0 "D3" V 4039 2682 50  0000 R CNN
F 1 "LED" V 3948 2682 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 4000 2800 50  0001 C CNN
F 3 "~" H 4000 2800 50  0001 C CNN
	1    4000 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 60B87C91
P 4000 2400
F 0 "R5" H 4070 2446 50  0000 L CNN
F 1 "10" H 4070 2355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 3930 2400 50  0001 C CNN
F 3 "~" H 4000 2400 50  0001 C CNN
	1    4000 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 60B87C97
P 4550 3350
F 0 "R6" V 4343 3350 50  0000 C CNN
F 1 "R1000" V 4434 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4480 3350 50  0001 C CNN
F 3 "~" H 4550 3350 50  0001 C CNN
	1    4550 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3050 4000 2950
Wire Wire Line
	4000 2650 4000 2550
$Comp
L Device:LED D4
U 1 1 60B89583
P 5150 3000
F 0 "D4" V 5189 2882 50  0000 R CNN
F 1 "LED" V 5098 2882 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 5150 3000 50  0001 C CNN
F 3 "~" H 5150 3000 50  0001 C CNN
	1    5150 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 60B89589
P 5150 2600
F 0 "R7" H 5220 2646 50  0000 L CNN
F 1 "10" H 5220 2555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5080 2600 50  0001 C CNN
F 3 "~" H 5150 2600 50  0001 C CNN
	1    5150 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 60B8958F
P 5700 3550
F 0 "R8" V 5493 3550 50  0000 C CNN
F 1 "R1000" V 5584 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 5630 3550 50  0001 C CNN
F 3 "~" H 5700 3550 50  0001 C CNN
	1    5700 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 3250 5150 3150
Wire Wire Line
	5150 2850 5150 2750
$Comp
L Device:LED D5
U 1 1 60B8B769
P 6150 2650
F 0 "D5" V 6189 2532 50  0000 R CNN
F 1 "LED" V 6098 2532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 6150 2650 50  0001 C CNN
F 3 "~" H 6150 2650 50  0001 C CNN
	1    6150 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R9
U 1 1 60B8B76F
P 6150 2250
F 0 "R9" H 6220 2296 50  0000 L CNN
F 1 "10" H 6220 2205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6080 2250 50  0001 C CNN
F 3 "~" H 6150 2250 50  0001 C CNN
	1    6150 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 60B8B775
P 6700 3200
F 0 "R10" V 6493 3200 50  0000 C CNN
F 1 "1000" V 6584 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 6630 3200 50  0001 C CNN
F 3 "~" H 6700 3200 50  0001 C CNN
	1    6700 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 2900 6150 2800
Wire Wire Line
	6150 2500 6150 2400
$Comp
L Device:LED D6
U 1 1 60B8D399
P 7250 3100
F 0 "D6" V 7289 2982 50  0000 R CNN
F 1 "LED" V 7198 2982 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 7250 3100 50  0001 C CNN
F 3 "~" H 7250 3100 50  0001 C CNN
	1    7250 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R11
U 1 1 60B8D39F
P 7250 2700
F 0 "R11" H 7320 2746 50  0000 L CNN
F 1 "10" H 7320 2655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7180 2700 50  0001 C CNN
F 3 "~" H 7250 2700 50  0001 C CNN
	1    7250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3350 7250 3250
Wire Wire Line
	7250 2950 7250 2850
$Comp
L Device:LED D7
U 1 1 60B8F715
P 8150 2650
F 0 "D7" V 8189 2532 50  0000 R CNN
F 1 "LED" V 8098 2532 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 8150 2650 50  0001 C CNN
F 3 "~" H 8150 2650 50  0001 C CNN
	1    8150 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R13
U 1 1 60B8F71B
P 8150 2250
F 0 "R13" H 8220 2296 50  0000 L CNN
F 1 "R10" H 8220 2205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 8080 2250 50  0001 C CNN
F 3 "~" H 8150 2250 50  0001 C CNN
	1    8150 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 60B8F721
P 8700 3200
F 0 "R14" V 8493 3200 50  0000 C CNN
F 1 "1000" V 8584 3200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 8630 3200 50  0001 C CNN
F 3 "~" H 8700 3200 50  0001 C CNN
	1    8700 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	8150 2900 8150 2800
Wire Wire Line
	8150 2500 8150 2400
Text GLabel 1350 2050 0    50   Input ~ 0
5V_USB
Wire Wire Line
	1150 4200 1700 4200
Wire Wire Line
	1700 4200 1700 3700
Connection ~ 1700 4200
Wire Wire Line
	8150 3500 8150 4200
Wire Wire Line
	7250 3950 7250 4200
Wire Wire Line
	6150 3500 6150 4200
Connection ~ 6150 4200
Wire Wire Line
	5150 3850 5150 4200
Connection ~ 5150 4200
Wire Wire Line
	5150 4200 6150 4200
Wire Wire Line
	4000 3650 4000 4200
Connection ~ 4000 4200
Wire Wire Line
	4000 4200 5150 4200
Wire Wire Line
	2700 3850 2700 4200
Wire Wire Line
	1700 4200 2700 4200
Connection ~ 2700 4200
Wire Wire Line
	2700 4200 4000 4200
Text HLabel 2400 3400 2    50   Input ~ 0
LED1
Text HLabel 3400 3550 2    50   Input ~ 0
LED2
Text HLabel 4700 3350 2    50   Input ~ 0
LED3
Text HLabel 5850 3550 2    50   Input ~ 0
LED4
Text HLabel 6850 3200 2    50   Input ~ 0
LED5
Text HLabel 8850 3200 2    50   Input ~ 0
LED7
Wire Wire Line
	8150 2050 8150 2100
Wire Wire Line
	7250 2550 7250 2050
Wire Wire Line
	6150 2100 6150 2050
Connection ~ 6150 2050
Wire Wire Line
	5150 2050 5150 2450
Connection ~ 5150 2050
Wire Wire Line
	5150 2050 6150 2050
Wire Wire Line
	4000 2250 4000 2050
Wire Wire Line
	1350 2050 1700 2050
Connection ~ 4000 2050
Wire Wire Line
	4000 2050 5150 2050
Wire Wire Line
	2700 2050 2700 2450
Connection ~ 2700 2050
Wire Wire Line
	2700 2050 4000 2050
Wire Wire Line
	1700 2300 1700 2050
Connection ~ 1700 2050
Wire Wire Line
	1700 2050 2700 2050
Connection ~ 7250 2050
Wire Wire Line
	7250 2050 8150 2050
Connection ~ 7250 4200
Wire Wire Line
	7250 4200 8150 4200
Wire Wire Line
	6150 4200 7250 4200
Wire Wire Line
	6150 2050 7250 2050
$Comp
L Device:R R12
U 1 1 60BCA897
P 7800 3650
F 0 "R12" V 7593 3650 50  0000 C CNN
F 1 "1000" V 7684 3650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 7730 3650 50  0001 C CNN
F 3 "~" H 7800 3650 50  0001 C CNN
	1    7800 3650
	0    1    1    0   
$EndComp
Text HLabel 7950 3650 2    50   Input ~ 0
LED6
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q1
U 1 1 60B9204D
P 2100 3400
AR Path="/60B9204D" Ref="Q1"  Part="1" 
AR Path="/60B89E55/60B9204D" Ref="Q1"  Part="1" 
F 0 "Q1" H 2638 3446 50  0000 L CNN
F 1 "2N2222AUB" H 2638 3355 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 2650 3250 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 2650 3150 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 2650 3050 50  0001 L CNN "Description"
F 5 "2.16" H 2650 2950 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 2650 2850 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 2650 2750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 2650 2650 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 2650 2550 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 2650 2450 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 2650 2350 50  0001 L CNN "Arrow Price/Stock"
	1    2100 3400
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q2
U 1 1 60B945CA
P 3100 3550
AR Path="/60B945CA" Ref="Q2"  Part="1" 
AR Path="/60B89E55/60B945CA" Ref="Q2"  Part="1" 
F 0 "Q2" H 3638 3596 50  0000 L CNN
F 1 "2N2222AUB" H 3638 3505 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 3650 3400 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 3650 3300 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 3650 3200 50  0001 L CNN "Description"
F 5 "2.16" H 3650 3100 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 3650 3000 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 3650 2900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 3650 2800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 3650 2700 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 3650 2600 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 3650 2500 50  0001 L CNN "Arrow Price/Stock"
	1    3100 3550
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q3
U 1 1 60B95350
P 4400 3350
AR Path="/60B95350" Ref="Q3"  Part="1" 
AR Path="/60B89E55/60B95350" Ref="Q3"  Part="1" 
F 0 "Q3" H 4938 3396 50  0000 L CNN
F 1 "2N2222AUB" H 4938 3305 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 4950 3200 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 4950 3100 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 4950 3000 50  0001 L CNN "Description"
F 5 "2.16" H 4950 2900 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 4950 2800 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 4950 2700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 4950 2600 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 4950 2500 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 4950 2400 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 4950 2300 50  0001 L CNN "Arrow Price/Stock"
	1    4400 3350
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q4
U 1 1 60B964F8
P 5550 3550
AR Path="/60B964F8" Ref="Q4"  Part="1" 
AR Path="/60B89E55/60B964F8" Ref="Q4"  Part="1" 
F 0 "Q4" H 6088 3596 50  0000 L CNN
F 1 "2N2222AUB" H 6088 3505 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 6100 3400 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 6100 3300 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 6100 3200 50  0001 L CNN "Description"
F 5 "2.16" H 6100 3100 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 6100 3000 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 6100 2900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 6100 2800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 6100 2700 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 6100 2600 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 6100 2500 50  0001 L CNN "Arrow Price/Stock"
F 12 "Q" H 5550 3550 50  0001 C CNN "Spice_Primitive"
F 13 "2N2222AUB" H 5550 3550 50  0001 C CNN "Spice_Model"
F 14 "Y" H 5550 3550 50  0001 C CNN "Spice_Netlist_Enabled"
	1    5550 3550
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q5
U 1 1 60B97407
P 6550 3200
AR Path="/60B97407" Ref="Q5"  Part="1" 
AR Path="/60B89E55/60B97407" Ref="Q5"  Part="1" 
F 0 "Q5" H 7088 3246 50  0000 L CNN
F 1 "2N2222AUB" H 7088 3155 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 7100 3050 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 7100 2950 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 7100 2850 50  0001 L CNN "Description"
F 5 "2.16" H 7100 2750 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 7100 2650 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 7100 2550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 7100 2450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 7100 2350 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 7100 2250 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 7100 2150 50  0001 L CNN "Arrow Price/Stock"
	1    6550 3200
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q6
U 1 1 60B97DCB
P 7650 3650
AR Path="/60B97DCB" Ref="Q6"  Part="1" 
AR Path="/60B89E55/60B97DCB" Ref="Q6"  Part="1" 
F 0 "Q6" H 8188 3696 50  0000 L CNN
F 1 "2N2222AUB" H 8188 3605 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 8200 3500 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 8200 3400 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 8200 3300 50  0001 L CNN "Description"
F 5 "2.16" H 8200 3200 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 8200 3100 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 8200 3000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 8200 2900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 8200 2800 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 8200 2700 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 8200 2600 50  0001 L CNN "Arrow Price/Stock"
	1    7650 3650
	-1   0    0    -1  
$EndComp
$Comp
L HAT_PCB-rescue:2N2222AUB-2N2222AUB Q7
U 1 1 60B98B50
P 8550 3200
AR Path="/60B98B50" Ref="Q7"  Part="1" 
AR Path="/60B89E55/60B98B50" Ref="Q7"  Part="1" 
F 0 "Q7" H 9088 3246 50  0000 L CNN
F 1 "2N2222AUB" H 9088 3155 50  0000 L CNN
F 2 "lib:JANTX2N2222A" H 9100 3050 50  0001 L CNN
F 3 "https://www.microsemi.com/document-portal/doc_download/8898-lds-0060-datasheet" H 9100 2950 50  0001 L CNN
F 4 "Bipolar Transistors - BJT Small-Signal BJT" H 9100 2850 50  0001 L CNN "Description"
F 5 "2.16" H 9100 2750 50  0001 L CNN "Height"
F 6 "Microsemi Corporation" H 9100 2650 50  0001 L CNN "Manufacturer_Name"
F 7 "2N2222AUB" H 9100 2550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "494-2N2222AUB" H 9100 2450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Microchip-Microsemi/2N2222AUB/?qs=TXMzd3F6EynaWACcclYa6A%3D%3D" H 9100 2350 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N2222AUB" H 9100 2250 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n2222aub/microsemi" H 9100 2150 50  0001 L CNN "Arrow Price/Stock"
	1    8550 3200
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 60B9AD87
P 1150 4200
F 0 "#PWR0116" H 1150 3950 50  0001 C CNN
F 1 "GND" H 1155 4027 50  0000 C CNN
F 2 "" H 1150 4200 50  0001 C CNN
F 3 "" H 1150 4200 50  0001 C CNN
	1    1150 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
